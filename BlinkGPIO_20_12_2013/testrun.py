import RPi.GPIO as GPIO
from Adafruit_I2C import *
from time import *
import os

DEBUG = 1

GPIO.setmode(GPIO.BCM)

def blink():
    for i in range(0,4):
        i2c.bus.write_byte(i2c.address, 0x80|0x40|pattern1[i%len(pattern1)])
        sleep(0.150)

def getTimeStamp():
    return strftime("%d-%m-%Y") + "__" + strftime("%H:%M:%S")


def button1_handler():
    print "Button 1 pressed"
    try:
        #Webcam video0
        i2c.bus.write_byte(i2c.address, 0x80|0x40|0b111011)
        print "check"
        
        res  = os.system("uvccapture -m -d'/dev/video0' -o'/home/pi/testoutput/Webcam0_{0}.jpg'".format(getTimeStamp()))
        sleep(1)
        
        res  = os.system("uvccapture -m -d'/dev/video0' -o'/home/pi/testoutput/Webcam0_{0}.jpg'".format(getTimeStamp()))
        if res==0:
            i2c.bus.write_byte(i2c.address, 0x80|0x40|0b111110)
        else:
            for i in range(0,3):
                i2c.bus.write_byte(i2c.address, 0x80|0x40|0b111011)
                sleep(0.1)
                i2c.bus.write_byte(i2c.address, 0x80|0x40|0b111111)
                sleep(0.1)
            return
        
        #Webcam video1
        i2c.bus.write_byte(i2c.address, 0x80|0x40|0b110110)
        
        res  = os.system("uvccapture -m -d'/dev/video1' -o'/home/pi/testoutput/Webcam1_{0}.jpg'".format(getTimeStamp()))
        sleep(1)
        res  = os.system("uvccapture -m -d'/dev/video1' -o'/home/pi/testoutput/Webcam1_{0}.jpg'".format(getTimeStamp()))
        if res==0:
            i2c.bus.write_byte(i2c.address, 0x80|0x40|0b011110)
        else:
            for i in range(0,3):
                i2c.bus.write_byte(i2c.address, 0x80|0x40|0b110110)
                sleep(0.1)
                i2c.bus.write_byte(i2c.address, 0x80|0x40|0b111110)
                sleep(0.1)
            return
        
        
    except:
        
        for i in range(0,4):
            i2c.bus.write_byte(i2c.address, 0x80|0x40|0b110011)
            sleep(0.1)
            i2c.bus.write_byte(i2c.address, 0x80|0x40|0b111111)
            sleep(0.1)
    sleep(3)
    loop()
    return
    

def button2_handler():
    print "Button 2 pressed"

def loop():
    while(1):
        i2c.bus.write_byte(i2c.address, 0x80|0x40|pattern1[0])
        sleep(0.150)
        i2c.bus.write_byte(i2c.address, 0x80|0x40|pattern1[1])
        sleep(0.150)
        val = i2c.bus.read_byte(i2c.address)
        if (val & (0x80|0x40)) == 0:
            break
        if (val & 0x80) == 0:
            button1_handler()
            break
        if (val & 0x40) == 0:
            button2_handler()
            break

try:
    i2c = Adafruit_I2C(address=0x38, busnum=1, debug=True)
    print "Successfully opened the PCF8574A at address 0x38"
except:
    print "Error accessing default I2C bus"

#           All off   Left-Red  Left-G    Right-G   Right-Red
patterns = [0b111111, 0b111011, 0b111110, 0b011111, 0b110111 ]

pattern1 = [0b111111, 0b011110]

for i in range(0,10):
    i2c.bus.write_byte(i2c.address, 0x80|0x40|patterns[i%len(patterns)]) 
    sleep(0.080)
    
blink()
blink()

loop()

#i2c.bus.write_byte(i2c.address,255)
print i2c.bus.read_byte(i2c.address)
print "Hello world!"



