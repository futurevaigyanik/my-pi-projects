import RPi.GPIO as GPIO
from Adafruit_I2C import *
from time import *

DEBUG = 1

#-----------TO CHECK FOR I2C Address-----
#
#  Use command: sudo i2cdetect -y 1
#
#----------------------------------------

GPIO.setmode(GPIO.BCM)

try:
    i2c = Adafruit_I2C(address=0x38, busnum=1, debug=True)
    print "Successfully opened the PCF8574A at address 0x38"
except:
    print "Error accessing default I2C bus"


for i in range(0,4):
    for j in range(0,64):
        i2c.bus.write_byte(i2c.address, j|0x80|0x40)
        val = i2c.bus.read_byte(i2c.address)
        if (val & 0x80)==0:
            print "Button 1 pressed"
        if (val & 0x40)==0:
            print "Button 2 pressed"
        sleep(0.050)
    
    
i2c.bus.write_byte(i2c.address,255)
print i2c.bus.read_byte(i2c.address)
print "Hello world!"
